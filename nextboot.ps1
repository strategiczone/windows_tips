<#
.SYNOPSIS
    List all available boot options and set the NextBoot firmware
    Microsoft documentation: https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/bcdedit-command-line-options
.NOTES
	File Name  : nextboot.ps1
	Author     : Magomed Gamadaev
	Version    : 0.1
.LINK
    https://gitlab.com/strategiczone/windows_tips/-/raw/master/nextboot.ps1
.EXAMPLE
    Open CMD as Admin and execute:
    powershell -exec bypass "iwr -useb https://gitlab.com/strategiczone/windows_tips/-/raw/master/nextboot.ps1 | iex"
    or
    powershell -exec bypass "iwr -useb urls.zone/nextboot | iex"
#>

bcdedit /enum firmware
Write-Host "`n`nExecute:`nbcdedit /set {fwbootmgr} bootsequence {xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}" -ForegroundColor green