<#
.SYNOPSIS
    Ncat is a feature-packed networking utility which reads and writes data across networks from the command line.
    Ncat was written for the Nmap Project as a much-improved reimplementation of the venerable Netcat.
    It uses both TCP and UDP for communication and is designed to be a reliable back-end tool to instantly
    provide network connectivity to other applications and users. Ncat will not only work with IPv4
    and IPv6 but provides the user with a virtually limitless number of potential uses. 
.NOTES
	File Name  : ncat.ps1
	Author     : Magomed Gamadaev
	Version    : 0.1
.LINK
    https://nmap.org/ncat/
    https://gitlab.com/strategiczone/windows_tips/-/raw/master/ncat.ps1
.EXAMPLE
    Open PowerShell and execute:
    iwr -useb https://gitlab.com/strategiczone/windows_tips/-/raw/master/ncat.ps1 | iex
    or
    iwr -useb urls.zone/ncat | iex
#>

$url = "https://pub.strat.zone/cF2Bw/ncat.exe"
$outpath = "$env:temp\ncat.exe"
Invoke-WebRequest -Uri $url -OutFile $outpath

cd $env:temp
Write-Host "`n`nExecute:`n.\ncat.exe -v 192.168.1.1 80" -ForegroundColor green