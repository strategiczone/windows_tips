# Windows_TIPS


## Change `Boot Order` of Windows machine via Command Line
Open `CMD` as Admin and execute:
```cmd
powershell -exec bypass "iwr -useb urls.zone/nextboot | iex"
```

## Ncat | Check connection to port
Open `PowerShell` and execute:
```powershell
iwr -useb urls.zone/ncat | iex
```
