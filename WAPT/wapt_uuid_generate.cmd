@echo off
goto :comment
Description: After deploymenet Windows to final machines we need to generate unique WAPT Agent UUID on each machines, using a WAPT administration account. This script do it.
Author: Magomed GAMADAEV
Version: 0.2
:comment

rem Varialbes
set wapt_user=admin
set wapt_pass=YOUR_ADMIN_PASSWORD

net stop WAPTService
wapt-get generate-uuid --wapt-server-user=%wapt_user% --wapt-server-passwd=%wapt_pass%
net start WAPTService