# WAPT Agent UUID Generate

## How TO
### Direct execute
 - Download and execute `wapt_uuid_generate.cmd` as `Admin`

### FOG Snapin
 - Downoad `wapt_uuid_generate.cmd` to `/opt/fog/snapins` on the `FOG server`
 - Import `wapt_uuid_generate.csv` via FOG Web Interface
 - Create `Single Snapin` task for machine or group by selecting `wapt_uuid_generate` snapin. 