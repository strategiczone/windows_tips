# Answer file - unattend.xml

## Keyboard layouts
* For SPAIN
```
        <component name="Microsoft-Windows-International-Core" processorArchitecture="amd64" publicKeyToken="31bf3856ad364e35" language="neutral" versionScope="nonSxS" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <UILanguage>en-GB</UILanguage>
            <InputLocale>0409:00020409,es-ES,fr-FR</InputLocale>
            <SystemLocale>en-GB</SystemLocale>
            <UserLocale>en-GB</UserLocale>
        </component>
```

* For FRANCE
```
        <component name="Microsoft-Windows-International-Core" processorArchitecture="amd64" publicKeyToken="31bf3856ad364e35" language="neutral" versionScope="nonSxS" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <UILanguage>fr-FR</UILanguage>
            <InputLocale>fr-FR;en-GB</InputLocale>
            <SystemLocale>fr-FR</SystemLocale>
            <UserLocale>fr-FR</UserLocale>
        </component>
```

## sysadmin password
* Replace `Value` in `<Password>` by your password. Refer to
```
            <UserAccounts>
                <LocalAccounts>
                    <LocalAccount wcm:action="add">
                        <Password>
                            <Value>bQB5AFMAdQBwAGUAcgBQAGEAcwBzAHcAbwByAGQAUABhAHMAcwB3AG8AcgBkAA==</Value>
                            <PlainText>false</PlainText>
                        </Password>
                        <Description>Sysadmin User</Description>
                        <DisplayName>sysadmin</DisplayName>
                        <Group>Administrators</Group>
                        <Name>sysadmin</Name>
                    </LocalAccount>
                </LocalAccounts>
            </UserAccounts>
```
* How to encode password into unattend.xml password format. Execute in PowerShell:
```powershell
$SysadminPassword = 'mySuperPassword'
$encodedSysadminPassword = [System.Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes(('{0}Password' -f $SysadminPassword)))
$encodedSysadminPassword
```

## TimeZone
* Replace `TimeZone`. Refer to: https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/default-time-zones
```
            <TimeZone>Romance Standard Time</TimeZone>
            <DisableAutoDaylightTimeSet>false</DisableAutoDaylightTimeSet>
```

## GHOST Image version
* Replace `Model` version
```
            <OEMInformation>
                <Manufacturer>Customized by Strategic Zone</Manufacturer>
                <Model>v1.0.0</Model>
                <SupportURL>https://bugs.strat.zone</SupportURL>
                <Logo>C:\Windows\oem\stratzone_logo.bmp</Logo>
                <SupportHours>UTC+1 / 09h00 - 19h00 5/7</SupportHours>
            </OEMInformation>
```
