@echo off
rem Delete All Windows 10 Apps
powershell "Get-AppxPackage -AllUsers | where-object {$_.name -notlike '*store*' -And $_.packagename -notlike '*culator*'} | Remove-AppxPackage"
powershell "Get-appxprovisionedpackage -online | where-object {$_.packagename -notlike '*store*' -And $_.packagename -notlike '*culator*'} | Remove-AppxProvisionedPackage -online"
rem Other
powercfg -h off
del /F c:\windows\system32\sysprep\panther\setupact.log
del /F c:\windows\system32\sysprep\panther\setuperr.log
del /F c:\windows\system32\sysprep\panther\ie\setupact.log
del /F c:\windows\system32\sysprep\panther\ie\setuperr.log
del /F "C:\Program Files (x86)\FOG\fog.log"
del /F "C:\Program Files (x86)\FOG\token.dat"

mkdir C:\Windows\Setup\scripts\
copy SetupComplete.cmd C:\Windows\Setup\scripts\ /Y
copy unattend.xml C:\Windows\System32\Sysprep\ /Y

rem OEM Wallpaper copy
mkdir C:\Windows\oem\
copy stratzone_logo.bmp C:\Windows\oem\stratzone_logo.bmp

rem Reset AnyDesk ID
net stop AnyDesk-04a43abf_msi
del /F /S /Q %programdata%\AnyDesk

net stop FOGService
sc config FOGService start= disabled
cleanmgr /sagerun:1
c:\windows\system32\sysprep\sysprep.exe /oobe /generalize /shutdown /unattend:c:\windows\system32\sysprep\unattend.xml
