# Hide errors
$ErrorActionPreference= 'silentlycontinue'

# Vars
$FogServerIP = "10.10.10.10"
$FogClientURL = "http://$FogServerIP/fog/client/download.php?newclient"
$FogEXE = "FOGService.msi"
$FogConfigFile = "C:\Program Files (x86)\FOG\settings.json";
$FogConfigString = "$FogServerIP";
$FullPath = "$env:TEMP\$FogEXE"

# Check if is alredy installed
$Check = (Select-String -Path $FogConfigFile -Pattern $FogConfigString) -ne $null

If(-Not $Check) {
	(New-Object Net.WebClient).DownloadFile("$FogClientURL", "$env:TEMP\$FogEXE")
	msiexec /i $FullPath /passive USETRAY="0" HTTPS="0" WEBADDRESS="$FogServerIP" WEBROOT="/fog" ROOTLOG="0"
} else {
	Write-Host "FOG is alredy installed"
}
