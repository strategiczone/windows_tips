# Hide errors
$ErrorActionPreference= 'silentlycontinue'

# Vars
$SaltMinionUrl = "https://repo.saltstack.com/windows/Salt-Minion-Latest-Py3-AMD64-Setup.exe"
$SaltMinionExe = "Salt-Minion-Latest-Py3-AMD64-Setup.exe"
$masterURL = "salt.domain.lan"
$minionName = "$env:COMPUTERNAME.domain.lan"
$SaltMasterString = "master: $masterURL";
$SaltConfigFile = "C:\salt\conf\minion";

# Check if is alredy installed
$Check = (Select-String -Path $SaltConfigFile -Pattern $SaltMasterString) -ne $null

If(-Not $Check) {
	(New-Object Net.WebClient).DownloadFile("$SaltMinionUrl", "$env:TEMP\$SaltMinionExe")
	Start-Process "$env:TEMP\$SaltMinionExe" -ArgumentList "-U /S /master=$masterURL /start-service=1 /minion-name=$minionName"
} else {
	Write-Host "Salt Minion is alredy installed"
}