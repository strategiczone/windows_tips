@echo off
SET SMB_SERVER_IP=10.10.10.10
SET MCS_ENDPOINT=Sophos\Management Communications System\Endpoint\McsClient.exe
IF "%PROCESSOR_ARCHITECTURE%" == "x86" GOTO X86_PROG
IF NOT EXIST "%ProgramFiles(x86)%\%MCS_ENDPOINT%" GOTO INSTALL
exit /b 0

:X86_PROG
IF NOT EXIST "%ProgramFiles%\%MCS_ENDPOINT%" GOTO INSTALL
exit /b 0

:INSTALL
pushd \\%SMB_SERVER_IP%\installers
SophosSetup_Workstations_Solid.exe --quiet
Popd